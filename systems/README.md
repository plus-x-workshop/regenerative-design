# Systems

###

### A Systemic Problem

* Entire impact of product (inc upstream and downstream of manufacture)
* Role of product in ecosystem
* Second-order effects
* Interplay of design and business decisions

### Where to Act

You can have different impact at different levels of the system

{% hint style="info" %}
See [Points of Leverage](points-of-leverage.md)
{% endhint %}

Some levels may seem too big / or beyond the scope of many designers:

* \[\[Growth]]
* Business Models

### Activity: waste processing plant visit

Veolia

### Activity: Matter out of place

VIa James 2022-11-04

A tricky workshop format - untested with coporaes, but I could do the setup in advance.

Crime scene

Photos of things in the wrong place

Actual things on the table

How did it get here

Who takes responsibility at each stage

WHere shoudl it be Is there a good place

WHere does your reponsibility begin and end
