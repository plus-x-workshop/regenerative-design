---
description: How can we act sustainably in an economic system that demands constant growth?
---

# Growth

**Better conceptions of growth**

We need an alternative to the facile idea of ever-increasing GDP that captures the imagination: we all want to grow, and not stagnate. But economic growth is just a means to an end, and there are other ways to stimulate innovation, creativity and a nourishing life.

{% hint style="info" %}
See [_Flourish_](../resources/books.md#flourish-design-paradigms-for-our-planetary-emergency), by Pawlyn/Ichioka, p. 117 onwards
{% endhint %}

> In biology, "growth' generally refers to **a dynamic process of responding to an emerging situation, and to organisms** or ecosystems reaching their full potential. When instability or breakdown occurs it frequently results in a new order. "In other words," Capra and Henderson state "creativity- the generation of new forms - is a key property of all living systems. This means that all living systems develop; life continually reaches out to create novelty." By contrast, conventional economics often reduces 'growth' to a single, quantitative measure of GDP, which compresses the rich complexity of human potential and our interaction with the web of life into a simplistic measurement.

