---
description: >-
  Donella Meadows' idea about choosing where to intervene in an system for
  maximum impact
---

# Points of Leverage

[https://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/](https://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/)

So one day I was sitting in a meeting about how to make the world work better — actually it was a meeting about how the new global trade regime, NAFTA and GATT and the World Trade Organization, is likely to make the world work worse. The more I listened, the more I began to simmer inside. “**This is a HUGE NEW SYSTEM people are inventing!” I said to myself. “They haven’t the SLIGHTEST IDEA how this complex structure will behave,” myself said back to me. “It’s almost certainly an example of cranking the system in the wrong direction — it’s aimed at growth, growth at any price!! And the control measures these nice, liberal folks are talking about to combat it — small parameter adjustments, weak negative feedback loops — are PUNY!!!”**

Suddenly, without quite knowing what was happening, I got up, marched to the flip chart, tossed over to a clean page, and wrote:

**PLACES TO INTERVENE IN A SYSTEM**

(in increasing order of effectiveness)

1. Constants, parameters, numbers (subsidies, taxes, standards).
2. Regulating negative feedback loops.
3. Driving positive feedback loops.
4. Material flows and nodes of material intersection.
5. Information flows.
6. The rules of the system (incentives, punishments, constraints).
7. The distribution of power over the rules of the system.
8. The goals of the system.
9. The mindset or paradigm out of which the system — its goals, power structure, rules, its culture — arises.

Everyone in the meeting blinked in surprise, including me. “That’s brilliant!” someone breathed. “Huh?” said someone else.

I realized that I had a lot of explaining to do.
