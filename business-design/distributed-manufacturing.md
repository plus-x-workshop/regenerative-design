---
description: Also, on-demand models
---

# Distributed Manufacturing

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](../resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention")p. 123
{% endhint %}

#### Best practice

* Additive over Substractive&#x20;
* Reduced number of polymers
* Take-back systems incorporated to use as feedstock

See also: [Urban mining](../product-design-strategies/waste-as-a-resource.md#urban-mining)
