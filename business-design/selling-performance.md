# Selling performance

## Retained ownership / selling performance

Internalise the costs, and also the benefits ie the hopeful opposite of 'externalised costs'

e.g. \[\[Design for disassembly]]

What are the sectors or product types where this makes most sense? e.g. toll roads, special garment hire, rent-a-wash

How could other systems be redesigned so it makes more sense there? Rolls Royce - pay per mile

Warrantees are an admission of failure of the linear industrial model

The Performance Economy Paperback – 24 Feb. 2010 by W. Stahel (Author)



#### Example: Rolls Royce: Pay-per-mile



#### Example: Mitsubishi Lifts: pay-per-use&#x20;

{% hint style="info" %}
See [#the-handbook-to-building-a-circular-economy](../resources/books.md#the-handbook-to-building-a-circular-economy "mention")p.129
{% endhint %}



#### Example: Philips lighting: pay-per-lux

{% hint style="info" %}
See [#the-handbook-to-building-a-circular-economy](../resources/books.md#the-handbook-to-building-a-circular-economy "mention")p.128
{% endhint %}



#### Example: [Mud Jeans](https://mudjeans.eu/pages/lease-page)



#### Example: Zip Car

Hire a car instead of buying it - have to make it easy to accesss mobility (unlike traditional car hire)



#### Example: Parently

Subscribe & rent children's products ​

{% embed url="https://www.parently.se" %}

#### Example: Bundles

From [KTN 30 Ideas Cards](../resources/#30-ideas-cards):

> Bundles offer washing machines, tumble dryers, dishwashers, and coffee machines as a service.
>
> Working with Miele and Siemens the company provide the machine and charge per use. By connecting to the internet maintenance, repairs, and even replacements are facilitated and included in the price.
