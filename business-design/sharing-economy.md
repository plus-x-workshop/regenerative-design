# Sharing Economy

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](../resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention")Ch. 7, p. 109
{% endhint %}

Note caution about consumer relationship to products (p.121)



### Example: Library of things

{% embed url="https://www.libraryofthings.co.uk" %}

Note how the customer experience needs to be configured around a complex set of considerations:

* Pricing
* [Safety and documentation](https://libraryofthings.notion.site/Drill-Borrower-Guide-65f848dc22bc4239a6e0a90f82269d78)
* [Guidance on experience needed, and alternative tools](https://www.libraryofthings.co.uk/catalogue/borrow-drill-cordless)
* Other related decisions that have impact (e.g. how to get it home)





