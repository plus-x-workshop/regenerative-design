# Incentivising return

Essential for:

* Repair
* Remanufacturing
* Design for disassembly
* Closed-loop recycling

Can offer manufactuer other benefits, such as information about common failure points on products, wear or usage patterns, types of uses.



#### Example: Rapanui

{% embed url="https://rapanuiclothing.com/circular-clothing/?overlay-remill-return-popup=1" %}

> **Return an item**
>
> If you have an item made by us, you can fill out this form to return it to us. It's free to do, and we pay the postage in the UK. We'll recover and remanufacture the material into a new product, and we'll award you with a coupon that you can use to save money on your next purchase.



### Can it be posted?

#### Back by the customer to you&#x20;

Agency of design toaster

#### Or a replacement to the customer?

From [KTN 30 Ideas Cards](../resources/#30-ideas-cards):

> BT Router - BT redesigned their home hub to fit through their customers’ letterbox. This lowers their delivery costs as they never need to make a second delivery attempt and it also allows the product to be easily returned when it’s time to upgrade.
