# Investment and finance

#### Finance for the Green Business and the Circular Economy, Bocconi University, Italy

{% embed url="https://didattica.unibocconi.eu/ts/tsn_anteprima2006.php?cod_ins=20553&anno=2018&IdPag=6067" %}

Students who attend this course develop in-depth knowledge on business model structure and financial analysis regarding deal typologies applied to the green business and the circular economy. Course outline:

* Green business and circular economy: what they are and what they do
* Green business and circular business models: in what ways they differ from linear business models and how they work
* Investment opportunities for outside investors. The concept of stranded linear assets
* The changing profile of economics, financials, and sources of volatility in the green business and the circular economy
* Which are the financial actors and the deal typologies involved? Corporate lending, corporate finance, investment banking, and asset management (C\&IB)
* How green and circular investments affect the main economics in the C\&IB business models
* Operating risk measures. Discrete approach and stochastic approach application to sources of volatility related to green and circular items
* Debt capital in green and circular deals: analysis of the risks and potential returns associated with the project/asset, financial sustainability, and debt-holders’ risk appetite (adequacy)
* Equity capital in green and circular deals: analysis of the risks and potential returns associated with the investment, equity risk, and return measures
* The future of green business and circular economy: a perspective
