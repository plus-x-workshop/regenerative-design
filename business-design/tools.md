# Tools



## General Tools

### Circular Business Model Canvas

[https://www.circulardesignguide.com/post/circular-business-model-canvas](https://www.circulardesignguide.com/post/circular-business-model-canvas)

> Develop or redefine your business model from a circular design perspective.

### Working in Teams

[https://www.circulardesignguide.com/post/build-teams](https://www.circulardesignguide.com/post/build-teams)

> Build teams to strengthen knowledge and expertise, build your relationships with stakeholders, and make implementation a success. Align on a shared goal and define how you collaborate.

[https://www.circulardesignguide.com/post/buy-in](https://www.circulardesignguide.com/post/buy-in)

> Map your stakeholders, understand their perspectives, and create narratives to help them feel invested in your cause.

[https://www.circulardesignguide.com/post/alignment](https://www.circulardesignguide.com/post/alignment)

> Learn how to apply the principles of design thinking to ensure your organisation can support the change to circularity.

### Brand and communication

[https://www.circulardesignguide.com/post/brand-promise](https://www.circulardesignguide.com/post/brand-promise)

> Uncover which elements of circularity reinforce your brand purpose to hone your message to your customers. Build your brand around your circular innovation.

[https://www.circulardesignguide.com/post/narrative](https://www.circulardesignguide.com/post/narrative)

> Learn the basics of telling great stories around your product or service and how it relates to circularity. What is an immersive and emotional story that makes people feel invested in your brand?

### Systems

[https://www.circulardesignguide.com/post/circular-interventions](https://www.circulardesignguide.com/post/circular-interventions)

> Identify small, measurable opportunities to design for circularity. This will help you scaffold your approach to the project you’re about to take on.

[https://www.circulardesignguide.com/post/lead-with-user-centred-research](https://www.circulardesignguide.com/post/lead-with-user-centred-research)

> Understand the needs of everyone involved in the use cycle of your circular proposition(s) – the end users or beneficiaries, but also suppliers, manufacturers, retailers and others who may reuse your materials.
