# 💼 Business Design

##

##

### Service Flip Worksheet

[https://www.circulardesignguide.com/post/service-flip](https://www.circulardesignguide.com/post/service-flip)

> Imagine how you might turn common products into a service model. Could your product be transformed into something that takes on a new or unexpected service experience?

## Complementary currency

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](../resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention")p. 102
{% endhint %}

Webster, p102 Means of exchange between players acting in a system (eg. exchanging coffee grounds to mycelium growers)

Not taxed Other benefits? Investable by community? (see also: value of social capital) Can give value to unvalorised commodities (manure, spent brewers grain) Insulated from speculation and price volatility

## Sharing Economy

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](../resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention")p. 112
{% endhint %}

Webster p 112

Can make use of unused assets

* Spare room
* Lawnmower
* Clothes
* Car
* etc.

Rewards maintenance (again, selling performance)

p121 Caution (and opportunity) about our relationship with products and brands.



### Accounting



Triple Bottom Line

Certification requirements (e.g. B-Corp or Well standard)

[True Cost Accounting](https://www.ecocostsvalue.com/true-cost-accounting/) (e.g. IdeMat 'ecocost')

### Investment and Social Capital

Are your goals compatible with different kinds of funding?

From [https://technation.io/climate-tech-report-2022/#introduction:](https://technation.io/climate-tech-report-2022/#introduction)

> ... it has been pleasing to see the start of a much needed shift in the distribution and patience of venture capital, coupled with increased support from the public sector towards both R\&D intensive companies and those working within hard to abate sectors, often where the most impact lies. However, more must be done to safeguard companies from falling into the valley of death, experienced by many climate tech companies due to longer times before commercialisation.

From [https://www.beauhurst.com/blog/patient-capital/:](https://www.beauhurst.com/blog/patient-capital/)

> ### What is patient capital investment?
>
> Patient capital does not have a rigid definition, but generally refers to long-term investment, where investors are prepared to wait a considerable amount of time (3-5 years in some sectors, 10-15 years in others) before seeing any financial returns. For this reason, fund managers implementing a [patient capital strategy](https://www.researchgate.net/publication/308877992\_'What\_is\_patient\_capital\_and\_who\_supplies\_it') will maintain their investments even if they’re seeing short-term losses for the fund.

### Certification

{% hint style="info" %}
See [certifications.md](../resources/certifications.md "mention")
{% endhint %}



### Macro-economic trends

Newly emerging profitable businesses as e.g. commodity prices increase

{% hint style="info" %}
See [#a-new-dynamic-effective-business-in-a-circular-economy](../resources/books.md#a-new-dynamic-effective-business-in-a-circular-economy "mention")p.11, 13, 23
{% endhint %}

Example given there:  [https://montreal.lufa.com/en/about](https://montreal.lufa.com/en/about)

