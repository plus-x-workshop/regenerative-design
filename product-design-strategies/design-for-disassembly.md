# Design for disassembly

### Example: Ara Task Chair <a href="#take-back-must-be-built-in" id="take-back-must-be-built-in"></a>

{% embed url="https://www.orangebox.com/products/ARA" %}

From [KTN 30 Ideas Cards](../resources/#30-ideas-cards):

> Ara is the first task chair manufactured in Europe to achieve Cradle to Cradle accreditation. Ara is designed for rapid disassembly to facilitate cost effective remanufacturing. The back membrane attaches without fasteners and a single aluminium vertebra simplifies an often complex assembly.

### Take-back must be built in to the business model <a href="#take-back-must-be-built-in" id="take-back-must-be-built-in"></a>

In a system - why should a manufacturer do this, if the beneficiary is the recycler (assuming they even know about it)? Hence - start with a business model that rewards the manufacturer.

{% hint style="info" %}
See [business-design](../business-design/ "mention")
{% endhint %}

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](../resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention"), p.90, Retained ownership / selling performance
{% endhint %}

### Assembly methods: fixings and glue

#### Worn fixings contaiminating material (e.g. rusted nails)

#### One-way fixings (welding, glue, tape)

#### Accessible fixings

### Composite materials ("Monstrous Hybrids")

Bonded composites (eg food packaging)

Fibre-reinforced materials

Resin/waste composites

Surface finishes (paint, varnish)

### Documenting materials, components and dissasembly instructions

### Integrating with reapir systems

