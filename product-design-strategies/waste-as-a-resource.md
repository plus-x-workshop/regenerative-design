# Waste as a resource

### Urban mining



### Waste as nutrient/resource

#### Example: Smile Plastics

> We take waste plastics and other materials traditionally classed as waste and transform them through our unique processes into large scale, solid surface panels. We tend to focus on single-use plastic packaging and other materials that would usually find their way to landfill. We chose these plastics to disrupt the unsustainable industrial ecosystems that have become the norm whereby finite and useful materials have very short, single lives. ​

{% embed url="https://smile-plastics.com/our-story/" %}

#### Example: Marinatex

> MarinaTex is a versatile material that can be an alternative to plastic in a variety of applications. From bags to single-use packaging, MarinaTex has a variety of different applications. The transparent film is well suited for packaging and will biodegrade in a soil environment. The organic formula does not leach harmful chemicals and can be consumed, causing no harm to wildlife or humans. ​

{% embed url="https://www.marinatex.co.uk" %}

#### Example: Cardboard to Caviar

See [https://www.theguardian.com/uk/2003/feb/12/helencarter:](https://www.theguardian.com/uk/2003/feb/12/helencarter)

> At a time when the fishing industry and poachers have forced the sturgeon, which produces caviar eggs, to the brink of commercial extinction in the Caspian sea, an environmental group in Yorkshire is farming the fish and hoping to turn cardboard into caviar through a series of simple processes.
>
> First, waste cardboard boxes from local businesses are shredded and given to farms and equestrian centres to be used as horse bedding. When the stables are cleaned out, the waste is fed to worms in a composting pit. The fattened worms are fed to the sturgeon which will produce caviar.

### Waste platforms – connecting waste with new users

#### Example: Harvest Maps – documenting available materials for the start of a new cycle

Developed by architecture studio [Superuse](https://www.superuse-studios.com/projectplus/bluecity-offices/)

From [https://www.abitare.it/en/architecture/materials-technologies/2018/07/14/harvest-map-recycling-platform/](https://www.abitare.it/en/architecture/materials-technologies/2018/07/14/harvest-map-recycling-platform/)

> It’s an open source platform created in Holland by the architect Cesar Peeren of Superuse Studios that identifies, maps and makes available to architects, designers, craftsmen and creatives construction materials that come from demolitions, products that have reached their end-of-life, unsold inventory and stock, components recovered from demolished buildings and leftovers from industrial manufacturing.
>
> ###

See also [https://www.ilovefreegle.org/about](https://www.ilovefreegle.org/about)

### Material Passports – documenting materials specs for the end of the current cycle

{% hint style="info" %}
See [#flourish-design-paradigms-for-our-planetary-emergency](../resources/books.md#flourish-design-paradigms-for-our-planetary-emergency "mention") p. 129
{% endhint %}

Documenting materials in manufacture to maximise chnace they can be reclaimed in the next cycle
