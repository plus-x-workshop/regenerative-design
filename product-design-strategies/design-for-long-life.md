# Design for long life

### Technical Durability

#### Example: Davek Umbrellas

From [https://davek.co.uk/pages/lifetime-guarantee](https://davek.co.uk/pages/lifetime-guarantee)

> Davek umbrellas are built to last—with proper use, your umbrella should remain by your side for many years to come. But we understand that things happen. A cab door can accidentally slam on the shaft. Your cat decides to use it for a scratching post. Hurricanes happen. Not to worry—all Davek umbrellas are covered under our famous **Unconditional Lifetime Guarantee**: If your umbrella should fail to function properly at any time for any reason whatsoever, we will gladly repair or replace it for free (except shipping). We want this umbrella to be the last umbrella you will ever need.

###

### Emotional Durability

#### Example: Loake shoes

Also requires repair infrastructure (e..g soles are consumable) and maintenance of old eeequipment (eg lasts for shoes that are no longer offered for sale)

#### Example Leica Cameras

Part of the brand

### Rethinking durability

#### **Ise Jingu and the Pyramid of Enabling Technologies**

> Our buildings are often long lived things, capable of outlasting the society that produced them. The Egyptian Pyramids are still with us, thousands of years after the civilization that produced them collapsed. The shrine of Ise Jingu is an interesting contrast to this - the buildings there are always young, perpetually renewed by a thousand-year old cultural practice.

{% embed url="https://theprepared.org/features-feed/ise-jingu-and-the-pyramid-of-enabling-technologies" %}

ALso see: Fewer Better Things: [https://www.amazon.co.uk/Fewer-Better-Things-Hidden-Objects/dp/1632869640](https://www.amazon.co.uk/Fewer-Better-Things-Hidden-Objects/dp/1632869640)
