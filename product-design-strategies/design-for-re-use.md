# Design for re-use

### Reusability of consumables

Separate the durable item (packaging) from the consumable.

Only ship the part of the consumable that's needed (concentrates)

#### Example: Splosh cleaning products
