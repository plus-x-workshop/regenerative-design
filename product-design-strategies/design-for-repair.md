# Design for repair

### Skins / shearing layers concept

Applied mostly in architecture, but can be applied to some products with variable wear rates across the system (e.g battery replacement in electronics, or strap replacement in wristwatches)

From [https://en.wikipedia.org/wiki/Shearing\_layers](https://en.wikipedia.org/wiki/Shearing\_layers)

> The shearing layers concept views buildings as a set of components that evolve in different timescales; Frank Duffy summarized this view in his phrase: "Our basic argument is that there isn't any such thing as a building. A building properly conceived is several layers of longevity of built components" (quoted in (Brand, 1994)).

#### Strata Furniture system

From [https://www.dezeen.com/2017/06/26/rca-graduates-propose-strata-system-extend-furniture-lifecycle-design/](https://www.dezeen.com/2017/06/26/rca-graduates-propose-strata-system-extend-furniture-lifecycle-design/)

<figure><img src="../.gitbook/assets/strata-katrine-hesseldahl-victor-strimfors-design-furniture-graduates_dezeen_2364_col_5-1704x1136.jpg" alt=""><figcaption></figcaption></figure>

> [Royal College of Art](https://www.dezeen.com/tag/royal-college-of-art/) graduates Katrine Hesseldahl and Victor Strimfors have designed a modular [sofa](https://www.dezeen.com/tag/sofas) made from three distinct layers in an attempt to combat furniture waste.
>
> ...
>
> The system divides furniture up into a skin, middle and base layer.
>
> By allowing owners to swap out just the layers that are most likely to need changing – the cushion and covering – the duo hopes to offer a more sustainable alternative to throwaway homeware.

### Repair infrastructures

#### Customer

* See iFixit
* Fairphone
* [Ikea store spare parts](https://www.ikea.com/gb/en/customer-service/spare-parts/)



#### Manufacturer

#### Third Party

#### Increasing regulations will also force action

From: [https://www.theguardian.com/business/2022/nov/08/the-trend-to-mend-how-repair-shops-are-leading-a-fixing-revolution](https://www.theguardian.com/business/2022/nov/08/the-trend-to-mend-how-repair-shops-are-leading-a-fixing-revolution?CMP=Share\_iOSApp\_Other)

> Manufacturers of phones, tablets and laptops [now face legal obligations](https://www.theguardian.com/world/2020/mar/11/eu-brings-in-right-to-repair-rules-for-phones-and-tablets) to make their products easier to repair and reuse, under an EU recycling plan to lengthen the life of products, with only 40% of electronic waste in the zone thought to be recycled.



