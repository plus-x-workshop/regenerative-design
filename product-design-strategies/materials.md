# Materials

### Separate bio and technical material streams

{% hint style="info" %}
See [#the-butterfly-diagram-visualising-the-circular-economy](../resources/#the-butterfly-diagram-visualising-the-circular-economy "mention")
{% endhint %}

### Simplify

Nature only uses 5 polymers (Challenge here for additive manufacturing)

* Beetle sheel uses one polymer to handle all functions - we use differnt materials eg in packaging for each function

TODO ref Biomimicry book



#### Example: Aluminium drinks can

One material, one part

### Remove toxins

Eg removal of PVC

### Avoid 'monstrous hybrids' and composite materials

#### Example: Ketchup bottle cap

> &#x20;In Heinz’s squeezable upside-down ketchup bottle, the plastic cap is designed to dispense a standard blob of sauce without spilling. It works. But it can’t easily be recycled, so the Kraft Heinz Company decided to design an alternative. ... The original cap used a flexible silicone valve, something that was “very difficult to recycle,” says Jonah Smith, global head of ESG at Kraft Heinz. “It’s possible, it’s just more difficult.” Recycling facilities would have to be able to separate the silicone from the rest of the cap—something not terribly cost effective for a tiny scrap of material—and then they’d need somewhere to send it.

{% embed url="https://www.fastcompany.com/90805336/why-heinz-spent-185000-hours-redesigning-this-ketchup-bottle-cap" %}

###

###

###

### Additive vs Subtractive Manufacturing

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](../resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention") p. 127
{% endhint %}

Subtractive takes so much more energy:

* Look at the number of tools required, and waste/heat generated&#x20;
* Is nature ever subtractive? (maybe in decomposition phases....)



###

### Biological materials

Technical materials can be swapped for biological ones, which can often be cascaded through the system without loss in quality (keeping circles small) through composting/biodegradation

{% hint style="info" %}
See [#the-handbook-to-building-a-circular-economy](../resources/books.md#the-handbook-to-building-a-circular-economy "mention")p.92
{% endhint %}

#### Example: Biohm

From [https://www.biohm.co.uk/mycelium](https://www.biohm.co.uk/mycelium):

> We have produced a mycelium insulation panel that will be the world’s first accredited mycelium insulation product. We are also developing new products and alternative applications for mycelium.&#x20;
>
> Mycelium not only outperforms petrochemical/plastic based construction materials in thermal and acoustic insulation but, as a natural material, it is also safer and healthier. Mycelium does not contain the synthetic, resin-based compounds that can cause harmful toxic smoke and the quick spread of flames during a fire.
>
> To grow our mycelium, we identify commercial and agricultural by-products that would otherwise go to landfill. Our entire manufacturing process only has a positive impact on the environment, meaning it is completely regenerative. Our manufacturing process is estimated to be carbon-negative, sequestering at least 16 tonnes of carbon per month.

