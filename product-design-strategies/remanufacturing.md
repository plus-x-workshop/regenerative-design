# Remanufacturing

{% hint style="info" %}
See [#the-handbook-to-building-a-circular-economy](../resources/books.md#the-handbook-to-building-a-circular-economy "mention")p.123
{% endhint %}

Example products:

* Paint
* Raised floor tiles
* Office furniture

#### Example: Rype office

From: [https://www.rypeoffice.com/furniture/](https://www.rypeoffice.com/furniture/):

> #### 1. Refurbish/remanufacture clients’ **existing furniture**
>
> We audit clients’ existing furniture to determine what can be returned to as-new condition and present options to the design team on how it can fit with the new office look and feel. Powder coating in any RAL colour, reupholstering in stylish sustainable fabrics, resizing deaks/table frames and replacing tops transforms existing furniture.
>
> Our in-house remanufacturing expertise informs what can be remanufactured to meet quality, time and budget requirements.
>
> #### 2. Remanufacture **externally sourced** second life furniture
>
> We source from office clearances and remanufacture to a high standard popular office furniture made by Herman Miller/Knoll, Vitra, Senator/Allermuir, Steelcase/Orangebox and Haworth.
>
> This furniture looks and performs as new, but at a significant cost saving, both economically and environmentally. Remanufacturing also enables custom sizes, colours and finishes.
>
> #### 3. Rype’s **exclusive ranges** made from waste
>
> Our award-winning design team creates office furniture that is ergonomically excellent, hardwearing, and integrates waste materials. We manufacture locally using SMEs and Social Enterprises including the Merthyr Tydfil Institute for the Blind.
>
> #### 4. New furniture that **can be remanufactured**
>
> Sometimes it is not possible to source all furniture in the above three ways. Then we recommend new furniture that can be remanufactured for multiple lives, creating excellent return on investment as well as reducing carbon emissions in future offices.
>
> Remanufacturability is assessed by our own remanufacturing experts.

#### Example: Disposable cameras

Depends on closed-loop recycling, which is possible with cameras because customers have to return them to get th efilm developed.&#x20;

From KTN 30 Ideas Deck:

> Disposable cameras are largely perceived as a single use item. In reality, when they are returned to be developed, the internal components get reused for many lifecycles, with the customer getting the same reliable product. Kodak have recycled over 1.5 billion single use cameras, a recycling rate of 84%.
