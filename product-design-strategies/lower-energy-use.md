# Lower energy use

### In product manufacturing

### In product usage

#### Example: shower head that recycles hot water

> Our shower is circular. We simply filter and purify the water before looping it back to you. This is what we call sustainable showering, made simple.

{% embed url="https://designtaxi.com/news/420815/IKEA-To-Launch-An-Innovative-Shower-Solution-That-Recycles-Water/?mkt_tok=MjExLU5KWS0xNjUAAAGH4bS3bPsoa135fkAIDOivV-q81d4e0XiI64bVjEQLxp_bIwwsbmljCVyoAYFeuUYRiHYNJB2Od0_WNMxTzxOeDhoryYg2tQkfV6UCTeOBS3ZmE9k" %}

{% embed url="https://flow-loop.com/function/?utm_source=DesignTAXI&utm_medium=DesignTAXI&utm_term=DesignTAXI&utm_content=DesignTAXI&utm_campaign=DesignTAXI" %}
