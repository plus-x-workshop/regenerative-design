# Design for adaptability

### Adapting to changing circumstances

#### Example: Stokke High Chair

[https://www.stokke.com/GBR/en-gb/highchairs/tripp-trapp/1001.html](https://www.stokke.com/GBR/en-gb/highchairs/tripp-trapp/1001.html)

Can last throughout a child's life as they grow.

<figure><img src="../.gitbook/assets/SIIBS_Global_Concept-GrowlineNew (1).jpg" alt=""><figcaption></figcaption></figure>

#### Design factors

* Well-made (lasts a long time)
* Easy for the user to customise (doesn't rely on it being sent back)
* Adjustable in key dimensions faced by users (e.g. size for children products, colour or finish for fashion products)&#x20;

#### Example: Modular shelving

Relies on system being around for a long time. Value is increased if systems are interoperable.

* Vitsoe 606 Shelving System
* Elfa



### Adapting for other users

After the initial purchase&#x20;

e.g. heirlooms

### Adapting for multiple uses

### Modular design systems

Modularity within one product (e.g. Elfa shelving) or across different categories (Open Structures)

Standardisation can enable adaptation or re-use



#### Example: Beosound Level Speaker

> Level is the world's first Cradle to Cradle Certified speaker (bronze level). Cradle to Cradle is the global standard for products that are safe, circular and responsibly made. That means you can easily upgrade it as technology evolves, if the battery runs out or if you want to change the colors. Next year or a decade from now. That's part of future-proofing our products.

{% embed url="https://www.bang-olufsen.com/en/gb/speakers/beosound-level" %}
