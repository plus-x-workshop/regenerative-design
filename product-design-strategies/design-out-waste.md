# Design out waste

#### Example: Agency of Design Toaster Project

> What is the right thing to do with your old iron, radio, and kettle? They can’t go in your recyclables and taking them to the dump is a hassle. Perfectly sized for the household bin, this is where 90% of them end up. Valuable and finite resources get lost to landfill. ​

{% embed url="https://www.agencyofdesign.co.uk/projects/design-out-waste/" %}

### Waste in Packaging

#### Example: Smol&#x20;

> Innovative, concentrated formulations, sustainably packaged, delivered direct whenever you need them. All at a fair price. ​

{% embed url="https://smolproducts.com" %}
