# Recycling

Keeping value in the loop



### Closed loop recycling



#### Example: Lush closed loop container recycling

From: [https://www.lush.com/uk/en/a/bring-it-back-our-new-look-recycling-scheme](https://www.lush.com/uk/en/a/bring-it-back-our-new-look-recycling-scheme)

> Our refreshed recycling scheme, Bring It Back, launched in March 2021 and invites our customers in the UK\&I to return any of their Lush plastic packaging back to our shops for recycling. Not only that but Lush fans can also claim 50p/c towards their Lush shopping per qualifying item they bring back! This ‘deposit return’ will be added towards their total bill at the till-point on the same day.
>
> ...
>
> We’ve realised that the most responsible thing we can do is to get as much of our plastic packaging as possible into a loop we can control.
>
> So our challenge is simple . . . We need to own it!&#x20;
>
> Both figuratively and literally - we want to take ownership of our plastic packaging and the responsibility for ensuring our bottles, pots and tubs are given the new life we know they should have. By moving towards a deposit-style scheme, whereby Lush customers purchase their products, knowing that they are ‘renting’ the packaging and can simply return it when they are finished, we keep the responsibility of waste-reduction and resource recycling as our responsibility.&#x20;

