---
description: Courses, other repositories online
---

# 🎓 Courses and Collections

### Courses and MOOCs

#### Ellen McArthur Foundation

{% embed url="https://ellenmacarthurfoundation.org/resources/education-and-learning/circular-economy-courses" %}

#### edX: Circular Economy: An Introduction

{% embed url="https://learning.edx.org/course/course-v1:Delftx+CircularX+3T2022/home" %}

#### Circular Product Design Assessment

{% embed url="https://online-learning.tudelft.nl/courses/circular-product-design-assessment/" %}

#### Engineering Design for a Circular Economy

{% embed url="https://online-learning.tudelft.nl/courses/engineering-design-for-a-circular-economy/" %}

### Other online repositories

(like this one)

#### Sustainable Design Toolkit

I found many of the resources linked here in this toolkit maintained by Natalie Walsh:

{% embed url="https://www.notion.so/nataliewalsh/Sustainable-Design-Toolkit-340b02bc9fa44f5aa50eb8bc50cd49e9?p=b802afaf80b347318b61c2f10ab739f4&pm=c" %}

#### EMF Case Studies archive

Other people on the journey

{% embed url="https://ellenmacarthurfoundation.org/topics/circular-economy-introduction/examples" %}

#### Circular Design Workshop resources

For running your own exploratory or learnign design workshops

{% embed url="https://www.circulardesignguide.com/resources" %}

#### A Sustainable Design Handbook

> A sustainability guide for anyone involved in the design and production of physical things.

{% embed url="https://www.sustainabledesignhandbook.com" %}
