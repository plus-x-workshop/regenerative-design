---
description: Reading list
---

# 📚 Books

#### [The Circular Economy: A Wealth of Flows - 2nd Edition ](https://www.amazon.co.uk/gp/r.html?C=139N1T2SKF371\&K=38ON5XA06C65Y\&M=urn:rtn:msg:2022101712455609f5d3aeb1a44a67b5ef4d797a40p0eu\&R=2MWXE5BNCNV7J\&T=C\&U=https%3A%2F%2Fwww.amazon.co.uk%2Fdp%2F0992778468%2Fref%3Dpe\_27063361\_485629781\_TE\_item\&H=ND51QAMLJKFVDCBKAZUBRTNJLMOA\&ref\_=pe\_27063361\_485629781\_TE\_item)

Webster, Ken,MacArthur, Dame Ellen,Stahel, Walter

#### [Product Design and Sustainability: Strategies, Tools and Practice](https://www.amazon.co.uk/Product-Design-Sustainability-Strategies-Practice/dp/1138301981/ref=sr\_1\_1?keywords=product+design+sustainability\&qid=1668600633\&sprefix=product+design+and+sus%2Caps%2C54\&sr=8-1)

Jane Penty

#### [Flourish - Design Paradigms for Our Planetary Emergency](https://www.flourish-book.com)

Sarah Ichioka & Michael Pawlyn

#### [The Handbook to Building a Circular Economy](https://ribabooks.com/The-Handbook-to-Building-a-Circular-Economy\_9781859469545)

David Cheshire&#x20;

#### [Radical Matter](https://thamesandhudson.com/radical-matter-9780500295397)

Kate Franklin and Caroline Till

#### [A New Dynamic: effective business in a circular economy](https://ellenmacarthurfoundation.org/a-new-dynamic-effective-business-in-a-circular-economy)

By Ellen MacArthur Foundation (Editor)



Doughnut Economics



\<hr>

### Collections

#### Reading list from EdX course: CircularX Circular Economy: An Introduction

{% embed url="https://courses.edx.org/courses/course-v1:Delftx+CircularX+3T2022/8953c97ce8db474ab2194a46891c4258/" %}



