---
description: LCA tools, materials data and collections
---

# 📊 Tools

### **Life Cyle Analysis and Material datasets**

There are many different tools, methodologies and datasets. Some are tailored for specific inductries, or levels of detail. Some are proprietary, others open.

{% hint style="info" %}
See [#product-design-and-sustainability-strategies-tools-and-practice](books.md#product-design-and-sustainability-strategies-tools-and-practice "mention") p38, 46
{% endhint %}

From [Life Cycle Assessment Iyyanki V. Muralikrishna, Valli Manickam, in Environmental Management, 2017](https://www.sciencedirect.com/topics/earth-and-planetary-sciences/life-cycle-assessment):

> An LCA study consists of four stages:
>
> **Stage 1**: Goal and scope aims to define how big a part of product life cycle will be taken in assessment and to what end will assessment be serving. The criteria serving to system comparison and specific times are described in this step.
>
> **Stage 2**: In this step, inventory analysis gives a description of material and energy flows within the product system and especially its interaction with environment, consumed raw materials, and emissions to the environment. All important processes and subsidiary energy and material flows are described later.
>
> **Stage 3**: Details from inventory analysis serve for impact assessment. The indicator results of all impact categories are detailed in this step; the importance of every impact category is assessed by normalization and eventually also by weighting.
>
> **Stage 4**: Interpretation of a life cycle involves critical review, determination of data sensitivity, and result presentation.

#### openLCA Nexus

Database of LCA databases

{% embed url="https://nexus.openlca.org/databases" %}

#### Idemat / Ecocosts

Free and easy-to-use data sets for comparison of materials, transport and energy use.

IDEMAT (short for Industrial Design & Engineering MATerials database) is a compilation of LCI data of the Sustainable Impact Metrics Foundation, SIMF, a non-profit spinn-off of the Delft University of Technology. It is designed for the need of designers, engineers and architects in the manufacturing and building industry.

{% embed url="http://idematapp.com" %}

{% embed url="https://www.ecocostsvalue.com/eco-costs/" %}

#### CleanMetrics

> We offer carbon footprinting and life-cycle assessment (LCA) tools, life-cycle inventory (LCI) databases, and simulators. Our tools are designed to easily model, quantify and generate insights about the climate impacts of business operations, products, materials and processes.&#x20;

{% embed url="https://www.cleanmetrics.com/ToolsDatabases" %}

#### CarbonScope

> CarbonScope is an innovative and easy-to-use carbon modeling tool that can do everything from corporate greenhouse gas inventories to product life-cycle assessments. You don't have to be a carbon footprinting or LCA expert to use CarbonScope.

{% embed url="http://www.carbonscope.net" %}

#### 2030 Calculator

> The 2030 Calculator can be used by any product brand or manufacturer to quickly calculate the carbon footprint of their products based on the emissions created from manufacturing and transport up until the point of sale.

{% embed url="https://www.2030calculator.com" %}

#### Circulytics

> Circulytics supports a company’s transition towards the circular economy, regardless of industry, complexity, and size. Going beyond assessing products and material flows, this free company-level measuring tool reveals the extent to which a company has achieved circularity across its entire operations.

{% embed url="https://ellenmacarthurfoundation.org/resources/circulytics/overview" %}

{% hint style="info" %}
See also [Penty](books.md#product-design-and-sustainability-strategies-tools-and-practice), p 53
{% endhint %}

### [ ](http://idematapp.comhttps/www.ecocostsvalue.com/eco-costs/)Material Libraries

#### Matrec

Search for circular materials: renewable, bio-based or recycled

{% embed url="https://www.matrec.com/en/circular-materials" %}

#### MaterialWise

> MaterialWise provides data-driven insight to help you avoid problematic ingredients from the beginning of the design process. A project of [ChemFORWARD](https://www.chemforward.org/), this free screening tool consolidates global regulatory and authoritative lists to help you quickly identify and eliminate known chemicals of high concern from the beginning of the design process.

{% embed url="https://www.materialwise.org" %}

#### Granta Edupack

> Ansys Granta EduPack—formerly CES EduPack—is a unique set of teaching resources that help academics enhance courses related to materials across engineering, design, science and sustainable development.

{% embed url="https://www.ansys.com/products/materials/granta-edupack" %}

#### Materiom

> An open database of regenerative materials - what they're made of, how they're made, and their performance properties. Share your work and gain deeper insights from an international community of scientists, engineers, and designers by making an open data contribution.

{% embed url="https://materiom.org" %}
