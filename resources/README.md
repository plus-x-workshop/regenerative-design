# 🔗 Resources

### Subsections

{% content-ref url="books.md" %}
[books.md](books.md)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="tools.md" %}
[tools.md](tools.md)
{% endcontent-ref %}

{% content-ref url="networks.md" %}
[networks.md](networks.md)
{% endcontent-ref %}

{% content-ref url="certifications.md" %}
[certifications.md](certifications.md)
{% endcontent-ref %}

## The butterfly diagram: visualising the circular economy

{% embed url="https://ellenmacarthurfoundation.org/circular-economy-diagram" %}

<figure><img src="../.gitbook/assets/Butterfly-Infographic.png" alt=""><figcaption></figcaption></figure>

## Design Council Design Value Framework

https://www.designcouncil.org.uk/fileadmin/uploads/dc/Documents/Beyond%20Net%20Zero%20-%20A%20Systemic%20Design%20Approach.pdf

> Until now, Design Council’s Design Economy reports have focused on design’s financial and economic contribution when assessing the value of design. ... Value is now increasingly seen as plural and not just about financial worth. This shift in emphasis has led us to devise a framework to map and assess the value of design in its broadest sense.

## 30 Ideas Cards

From [https://www.innovationcanvas.ktn-uk.org/resources/30-ideas-to-kickstart-your-circular-business](https://www.innovationcanvas.ktn-uk.org/resources/30-ideas-to-kickstart-your-circular-business)

> This is a KTN tool to help businesses large and small design, develop and evaluate the business models of the future. Thirty ‘what if’ questions challenge the status quo and inspire new solutions to today’s problems.

## Sustainable Design Toolkit

From [https://www.notion.so/nataliewalsh/Sustainable-Design-Toolkit-340b02bc9fa44f5aa50eb8bc50cd49e9](https://www.notion.so/nataliewalsh/Sustainable-Design-Toolkit-340b02bc9fa44f5aa50eb8bc50cd49e9):

> The first section includes principles, methods, and tools to facilitate a more holistic systems mindset. The following section includes actionable resources by design discipline for applying these principles. The final section includes opportunities for further engagement via communities and deeper learning.

{% embed url="https://abettersource.org" %}

> A Directory of Environmentally Conscious Resources for Planet-loving Designers & Businesses

Lots of packaging idea and case studies



{% embed url="https://www.c2ccertified.org/resources/collection-page/cradle-to-cradle-certified-resources-public" %}

> Standards, guidance, methodologies, policies and forms supporting the Cradle to Cradle Certified® Products Program.

{% embed url="https://www.sustainabledesignhandbook.com/tools" %}

