# 📃 Certifications



### Cradle to Cradle

From: [https://www.c2ccertified.org/get-certified/product-certification](https://www.c2ccertified.org/get-certified/product-certification)

> Cradle to Cradle Certified assesses the safety, circularity and responsibility of materials and products across five categories of sustainability performance:
>
> * Material Health: ensuring materials are safe for humans and the environment
> * Product Circularity: enabling a circular economy through regenerative products and process design
> * Clean Air & Climate Protection: protecting clean air, promoting renewable energy, and reducing harmful emissions
> * Water & Soil Stewardship: safeguarding clean water and healthy soils
> * Social Fairness: respecting human rights and contributing to a fair and equitable society

### B Corp

Embed aligned values within the structure of the business. e.g. to avoid conflicts due to 'shareholder primacy'.  From [https://www.bcorporation.net/en-us/certification:](https://www.bcorporation.net/en-us/certification)

> In order to achieve certification, a company must:&#x20;
>
> * Demonstrate **high** **social and environmental** **performance** by achieving a B Impact Assessment score of 80 or above and passing our risk review. Multinational corporations must also meet baseline requirement standards.&#x20;
> * Make a **legal commitment** by changing their corporate governance structure to be accountable to all stakeholders, not just shareholders, and achieve benefit corporation status if available in their jurisdiction.&#x20;
> * Exhibit **transparency** by allowing information about their performance measured against B Lab’s standards to be publicly available on their B Corp profile on B Lab’s website. &#x20;
