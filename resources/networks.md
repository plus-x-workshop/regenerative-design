---
description: People acting together on sustainable design and climate
---

# 👯 Networks

{% embed url="https://climateaction.tech" %}

> As tech workers, we can use our skills to take and accelerate climate action. At ClimateAction.tech, we meet, discuss, learn and take climate action together.

{% embed url="https://designdeclares.com" %}

> Design Declares is a growing group of designers, design studios, agencies and institutions here to declare a climate and ecological emergency. As part of the global declaration movement, we commit to harnessing the tools of our industry to reimagine, rebuild and heal our world.



{% embed url="https://carbonliteracy.com" %}

> The Carbon Literacy Project offers everyone a day’s worth of Carbon Literacy learning, covering – climate change, carbon footprints, how you can do your bit, and why it’s relevant to you and your audience.

### LinkedIn groups

...
