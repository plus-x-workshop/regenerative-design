---
description: WiP docs for a regenerative design programme at Plus X in March 2023
---

# Regenerative Design Index

### Programme outline

#### Who is it for <a href="#_toc117675914" id="_toc117675914"></a>

* Product design-led businesses
* SMEs who want to make their physical products “sustainable” – to make better design decisions
* Designers, entrepreneurs, founders of early-stage businesses, but also…
* People working within more mature SMEs who want to make change within the business

#### What’s the need

People want to design products that have a positive impact on the planet, eliminate waste and create value throughout their lifespan. But they need to understand how to make informed decisions, how and where to act in the system, and what are the practical opportunities to act today.

#### What is it

Three days of inspiring learning about regenerative design, covering both knowledge of the principles and practical application to your own business.

#### Benefits/outcomes

Learn how to:

* Build a design-based business that can make net-positive products, where customers, suppliers and investors all benefit from a regenerative design approach.
* Measure the impact of your supply chain, the materials and processes used in manufacturing, the useful life of your products – and what happens to them next.
* Design out waste from your products; make them more repairable, adaptable and longer-lasting; and find new materials that make a positive contribution to the environment.

#### Draft Agenda

Day 1: Overview

* Designing in systems (Product lifecycles and ecosystems, waste exercise)
* Possibility of action (case studies, intro to design strategies, tools, materials)
* Clarity of focus (personal values, points of leverage, roadmap and first step, staying true to values)

Day 2: Design strategies in practice

* Designing out waste
* Design for disassembly, adaptation, repair
* Measuring impact. Data on materials and supply chain. Finding and assessing partners. Tools for lifecycle analysis

Day 3: Business strategies

* Protecting, enabling design decisions
* Circular business design patterns
* Implementing design strategies such as take-back, refill, repair, or closed loop recycling
* Investment incentives and timeframes
* Action plans for cohort
