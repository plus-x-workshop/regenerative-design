# Design Strategies

### Design for disassembly

In a system - why should a manufacturer do this, if the beneficiary is the recycler (assuming they even know about it)

Hence - start with a business model that rewards the manufacturer

\-> Retained ownership / selling performance Webster, p90

### Materials

### Separate bio and technical material streams

### Simplify

Nature only uses 5 polymers (Challenge for additive manufacturing)

Beetle sheel uses one polymer to handle all functions - we use differnt materials eg in packaging for each function

### Additive vs Subtractive

webster, p.127

Subtractive takes so much more energy Look at the number of tools required And waste generated Is nature ever subtractive? (maybe in decomposition phases....)

### Take-back must be built in
