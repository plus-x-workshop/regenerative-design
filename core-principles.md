# Core Principles

### Circular economy

Butterfly diagram

[Video](https://youtu.be/NBEvJwTxs4w)

{% embed url="https://youtu.be/NBEvJwTxs4w" %}

Linear and cyclical flows

Externalities

### Sustainable vs Regenerative

Regenerate natural systems

[EMF](https://emf.thirdlight.com/link/mk4v9gsntsgm-63ocgv/@/preview/1?o):&#x20;

> What if we could not only protect but actively improve the environment? In nature, there is no concept of waste. Everything is food for something else – a leaf that falls from a tree feeds the forest. We can take inspiration from living systems by designing healthy outputs that add value to the biosphere rather than degrade it. With a regenerative mindset, we can aim to do good rather than just be ‘less bad’.

#### Efficiency&#x20;

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention")p.15
{% endhint %}

### Waste

Both designing from waste and designing out waste

### Bio and technical material cycles



### Optimising cycles

Keeping value in materials and products

Heirachy of strategies/materials.

EMF:&#x20;

> The focus of a circular economy is on maintaining products, components, and materials at their highest possible value for the longest possible time through reuse, repair, refurbishment, and remanufacturing strategies. Recycling is part of the circular economy, but it represents the outermost loop on the ‘butterfly diagram’, often referred to as the ‘loop of last resort’, when other options for products and materials are no longer available.



### Diversity

For creativity and resilience.



### Renewable Energy

Throughout product lifecycle

"Current solar income" (Cradle to Cradle)&#x20;





### Lifecycle



### CO2, GHG, pollutants and other harms





### Biomimicry

### Perspectives

#### The Enlightenment view

{% hint style="info" %}
See [#the-circular-economy-a-wealth-of-flows-2nd-edition](resources/books.md#the-circular-economy-a-wealth-of-flows-2nd-edition "mention")p.58
{% endhint %}

#### Long-term vs short-term thinking



### Systems

#### [Leverage](systems/points-of-leverage.md)



